import json

import numpy as np
from matplotlib import pyplot as plt


# computes MSE
def mean_squared_error(prediction, real_value):
    return np.mean(np.square(np.array(real_value) - np.array(prediction)))/100


# computes RMSE
def root_mean_squared_error(prediction, real_value):
    return np.sqrt(np.mean(((prediction - real_value) ** 2)))/100


# computes MAE
def mean_absolute_error(prediction, real_value):
    return np.mean(np.abs(np.array(real_value) - np.array(prediction)))/100


# Computes profit if model could trade in virtual trading.
# Does not take spread or fees into account.
# Computes profit as:
# Virtual trader has one milion euros. For each trade uses 10.000 euros.
# Trades every day,by buying or selling (shorting) instrument at the start of the day
# and selling (buying back) at the end of the day.
# Secondly is computed profit for whole instrument by adding profit of each day.
def calculate_profit(y_test, predictions, ds_name, model_name):
    # pre-defined balances
    bet_value = 10_000
    balance = 1_000_000
    balance_delta = 0
    balance_without_predictions = 1_000_000
    balance_without_predictions_delta = 0

    balance_history = []
    balance_history_without_predictions = []
    predictions_length = len(predictions)

    for i in range(1, predictions_length):
        prediction = predictions[i]
        prediction_day_before = predictions[i-1]

        real_value = y_test[i]
        real_value_day_before = y_test[i - 1]

        predicted_percentage_change = ((prediction - prediction_day_before) / prediction_day_before)

        # say price rose from 200 to 102 - so delta will be 0.2
        # if we invested 100$ (bet_value) - at the end of the day we would win 20$
        # in case 200 yesterday -> 160 - delta = -0.2 - value = -20
        delta = (real_value - real_value_day_before) / real_value_day_before
        win_lose_value = int(delta * bet_value)

        balance_without_predictions += win_lose_value

        # we could set some threshold, but in that case
        # we would never invest if predicted is just stationary line
        balance += (win_lose_value if (predicted_percentage_change > 0.0) else win_lose_value*(-1))

        # if i > (predictions_length-250):
        balance_delta += (delta if (predicted_percentage_change > 0.0) else delta*(-1))
        balance_without_predictions_delta += delta

        balance_history_without_predictions.append(balance_without_predictions)
        balance_history.append(balance)

    # return balance_history
    print('Balance with strategy with this model:' + str(balance))
    print('Balance with long-term strategy:' + str(balance_without_predictions))

    # save results
    if model_name is not None:
        profit_per_step = (balance - 1_000_000) / predictions_length

        normalizing_interval_koeficient = 7
        if ds_name.startswith('daily'):
            normalizing_interval_koeficient = 7
        elif ds_name.startswith('60min'):
            normalizing_interval_koeficient = 7*24
        elif ds_name.startswith('30min'):
            normalizing_interval_koeficient = 7*24*2
        elif ds_name.startswith('1min'):
            normalizing_interval_koeficient = 7*24*60

        results = {
            ds_name + '-' + model_name + '-balance': balance,
            ds_name + '-' + model_name + '-balance_without_predictions': balance_without_predictions,
            ds_name + '-' + model_name + '-length': predictions_length,
            ds_name + '-' + model_name + '-profit_per_step': profit_per_step,
            ds_name + '-' + model_name + '-normalized_profit': profit_per_step * normalizing_interval_koeficient,
            ds_name + '-' + model_name + '-delta': balance_delta.item() / predictions_length,
            ds_name + '-' + model_name + '-delta_without_predictions':
                balance_without_predictions_delta.item() / predictions_length
        }
        with open('data-results/' + ds_name+'-'+model_name + 'data-balance.json', 'w') as fp:
            json.dump(results, fp,  indent=4)

    # plot the profits
    plt.figure(figsize=(16, 6))
    plt.title('Model')
    plt.plot(balance_history, color='blue', label='Model balance')
    plt.plot(balance_history_without_predictions, color='red', label='Long term balance')
    plt.xlabel('Steps', fontsize=18)
    plt.ylabel('Balance', fontsize=18)

    plt.legend()

    # save plots if needed
    if model_name is not None:
        plt.savefig('plots/' + ds_name + '-' + model_name + '-profit.png', bbox_inches='tight')

    plt.show()
