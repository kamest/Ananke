import os
from configparser import ConfigParser

from matplotlib import pyplot as plt


# Loads api keys for dataset services.
def load_config():
    config = ConfigParser()
    config.read('config/config.ini')
    return config.get('alphavantage', 'api_key'), config.get('binance', 'api_key'), config.get('binance', 'api_secret')


# Created data structure for datasets.
def create_dir_structure():
    os.mkdir('./data')
    os.mkdir('./data/1min')
    os.mkdir('./data/30min')
    os.mkdir('./data/60min')
    os.mkdir('./data/daily')


# Plots two datasets and optionally saves the plot into image (if model_name is set).
def plot_two_series(first_series, first_series_title, second_series, second_series_title, date_series, model_name=None):
    data_point_scale = int(first_series.shape[0] / 12)

    plt.figure(figsize=(16, 6))
    plt.title('Model')
    plt.plot(first_series, color='cyan', label=first_series_title)
    plt.plot(second_series, color='purple', label=second_series_title)
    plt.xlabel('Datum', fontsize=18)
    plt.ylabel('Cena', fontsize=18)

    if date_series is not None:
        dates = date_series[len(date_series) - len(first_series):]
        plt.xticks(
            range(0, dates.shape[0], data_point_scale),
            dates.loc[::data_point_scale],
            rotation=45)

    plt.legend()

    if model_name is not None:
        plt.savefig('plots/' + model_name + '.png', bbox_inches='tight')

    plt.show()
