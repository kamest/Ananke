# Ananke

This repository is part of the diploma thesis : Robo-investing

[FIM-UHK Czech Republic](https://www.uhk.cz/en/faculty-of-informatics-and-management/about-faculty)

Author: Stepan Kamenik (kamenik.stepan@gmail.com)

**!Important: Model files are in lfs storage, so you need to proceed `git lfs fetch`**

This git repository contains the application code described in the practical part of diploma thesis Robo-investing.
The main work code is contained in the file **work.ipynb**, in which it is possible to find brief documentation
for the code. The mentioned code uses functions from the util folder, which simplifies work in the file. These are
mainly functions that download transformation, evaluation (testing) and visualization (displaying graphs) data. Data is
downloaded using the services described in the data_services chapter of the work. These services require access api
keys, so
it is necessary to specify these keys in the config folder. The data is then downloaded to the **data** folder.

The main jupyter notebook allows you to create and test individual models, but testing all datasets on all models
would be impractical in a jupyter notebook (due to stored output). Therefore, the file **tests.py** is located in the
root folder, which, using the same functions (from the util folder), automatically creates all presented models and
tests on all downloaded data sets. The results are then stored in several folders for each additional observation: **
data-results** contains the metric results of the models on the individual data sets, including the results of
virtual trading in a separate **json** files. Another output is the **plots** folder containing graphs of
individual results. To be able to repeat the measurement, the **models** folder is created when the script is run
for the first time, which contains all learned models, so it is not necessary to complete the learning process again and
the testing time is reduced to units of minutes (instead of units of hours if learning is necessary). The override
function of saved models can be turned off by adding the parameter `train_new_model = True` to the
function `eval_model`.

The last of the usable script files is also jupyter notebook - **result_data.ipynb**. This notebook contains loading of
all json files with results and experiments with them. It contains several ready-made functions for displaying general
results and creating graphs for these results

The Git repository contains created and learned **h5** models, json files and graph images. These additional files are
stored in the lfs repository of the git repository. For licensing reasons, the data itself are not located in the git
repository and must be generated from the listed services (executed by a script in the notebook **work.ipynb**).