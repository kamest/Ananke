# finance apis
import json

from binance.enums import *
from keras import Model, layers
from keras import Sequential
from keras import regularizers
from keras.layers import *

# custom scripts
from util.download import *
from util.ml_util import *
from util.util import *

# ml util
globals()['evals'] = {}

# indicates whether intraday data should be evaluated (are the most time-consuming datasets) - True = Skip.
skip_intraday = False
# Specifying this variable, evaluation will take only this ticker.
only_ticker = None

# download the data
if not os.path.exists('./data'):
    print("Data not found, downloading it!")
    api_key, binance_api_key, binance_api_secret = load_config()

    download_daily_data(only_ticker)
    if not skip_intraday:
        for interval in ['1min', '30min', '60min']:
            download_intraday_data(interval, api_key, only_ticker)
        for interval in [KLINE_INTERVAL_1MINUTE, KLINE_INTERVAL_30MINUTE, KLINE_INTERVAL_1HOUR]:
            download_crypto_data(interval, binance_api_key, binance_api_secret, only_ticker)
else:
    print("Data already exists, enjoy it!")

base_dir = './data'

# load the data
ds_dict = {}
for interval_dir in os.walk(base_dir):
    if interval_dir[0] != base_dir:
        for ds_in_dir in os.walk(interval_dir[0]):
            for ds in ds_in_dir[2]:
                path_to_dataset = os.path.join(ds_in_dir[0], ds)
                dataset = pd.read_csv(path_to_dataset)
                ds_dict[ds_in_dir[0].split('/')[-1] + '_' + ds] = dataset.iloc[:, 1:]


# prepare method for combined model
def prepare_dataset_with_indicators(local_ds_dict, ds_name, inner_ds_split_ratio):
    local_df = local_ds_dict[ds_name]
    local_df = local_df[int(len(local_df) * inner_ds_split_ratio):]

    inner_dataset = {'close': local_df['close']}

    # Inspired, but changed from : https://github.com/borisbanushev/stockpredictionai#arimafeature
    # Create 7 and 21 days Moving Average
    inner_dataset['ma7'] = inner_dataset['close'].rolling(window=7).mean()
    inner_dataset['ma21'] = inner_dataset['close'].rolling(window=21).mean()

    # Create MACD
    inner_dataset['26ema'] = inner_dataset['close'].ewm(span=26, adjust=False, min_periods=12).mean()
    inner_dataset['12ema'] = inner_dataset['close'].ewm(span=12, adjust=False, min_periods=12).mean()
    inner_dataset['MACD'] = (inner_dataset['12ema'] - inner_dataset['26ema'])

    # Create Bollinger Bands
    inner_dataset['20sd'] = inner_dataset['close'].rolling(20).std()
    inner_dataset['upper_band'] = inner_dataset['ma21'] + (inner_dataset['20sd'] * 2)
    inner_dataset['lower_band'] = inner_dataset['ma21'] - (inner_dataset['20sd'] * 2)

    # Create Exponential moving average
    inner_dataset['ema'] = inner_dataset['close'].ewm(com=0.5).mean()

    # Create Momentum
    inner_dataset['momentum'] = inner_dataset['close'] - 1
    return local_df, DataFrame(inner_dataset).fillna(0)


# for each dataset
for key, value in ds_dict.items():
    print('evaluating ' + key)

    # Evaluate ARIMA model
    ds_split_ratio = 0.2
    if key.startswith('1min'):
        ds_split_ratio = 0.96
    if key.startswith('1min_BTC') or key.startswith('1min_ETH'):
        ds_split_ratio = 0.99

    print('evaluating ' + key)
    evaluate_arima(ds_dict, key, ds_split_ratio)

    # Evaluate ANN model
    ds_split_ratio = 0.0
    if key.startswith('1min') or key.startswith('30min'):
        ds_split_ratio = 0.5
    length_of_sample = 5

    model = Sequential()
    model.add(Dense(units=128, activation='relu', input_dim=length_of_sample))
    model.add(Dense(units=256, activation='relu'))
    model.add(Dense(units=1, activation='linear'))

    ds_split_ratio = 0.2
    if key.startswith('1min') or key.startswith('30min'):
        ds_split_ratio = 0.5

    eval_model(
        ds_dict,
        ds_name=key,
        ds_split_ratio=ds_split_ratio,
        scaler=MinMaxScaler(feature_range=(0, 1)),
        train_ratio=0.97,
        length_of_sample=length_of_sample,
        model=model,
        opt='adam',
        bs=200,
        epochs=50,
        model_name='ANN')

    # Evaluate LSTM model
    length_of_sample = 60

    model = Sequential()
    model.add(LSTM(128, return_sequences=True, input_shape=(length_of_sample, 1)))
    model.add(LSTM(64, return_sequences=False))
    model.add(Dense(25))
    model.add(Dense(1))

    eval_model(
        ds_dict,
        ds_name=key,
        ds_split_ratio=ds_split_ratio,
        scaler=MinMaxScaler(feature_range=(0, 1)),
        train_ratio=0.95,
        length_of_sample=length_of_sample,
        model=model,
        opt='adam',
        bs=32,
        epochs=40,
        model_name='basic_LSTM')

    # Evaluate Bi-LSTM model
    length_of_sample = 60
    model = Sequential()
    model.add(
        Bidirectional(
            LSTM(128, return_sequences=False),
            input_shape=(length_of_sample, 1)))
    model.add(Dense(16))
    model.add(Dense(1))
    bs = 100
    if key.startswith('1min'):
        bs = 300

    eval_model(
        ds_dict,
        ds_name=key,
        ds_split_ratio=ds_split_ratio,
        scaler=MinMaxScaler(feature_range=(0, 1)),
        train_ratio=0.95,
        length_of_sample=length_of_sample,
        model=model,
        opt='adam',
        bs=bs,
        epochs=20,
        model_name='bi-lstm')

    # Evaluate Ensemble model
    input1 = Input(shape=(length_of_sample, 1))

    first = Bidirectional(LSTM(128, return_sequences=False), input_shape=(length_of_sample, 1))(input1)
    first = Dense(16)(first)

    second = Bidirectional(LSTM(128, return_sequences=False), input_shape=(length_of_sample, 1))(input1)
    second = Dense(16)(second)

    adding_model = layers.concatenate([first, second], axis=1)
    adding_model = Dense(32)(adding_model)
    adding_model = Dense(1)(adding_model)

    model = Model(inputs=input1, outputs=adding_model)
    model.summary()

    bs = 500
    if key.startswith('60min'):
        bs = 100
    if key.startswith('daily'):
        bs = 10
    eval_model(
        ds_dict,
        ds_name=key,
        ds_split_ratio=ds_split_ratio,
        scaler=MinMaxScaler(feature_range=(0, 1)),
        train_ratio=0.95,
        length_of_sample=length_of_sample,
        model=model,
        opt='adam',
        bs=bs,
        epochs=20,
        model_name='ensemble')

    # Evaluate Ensemble model with only simple LSTM modules
    input1 = Input(shape=(length_of_sample, 1))

    first = LSTM(128, return_sequences=False, input_shape=(length_of_sample, 1))(input1)
    first = Dense(16)(first)

    second = LSTM(128, return_sequences=False, input_shape=(length_of_sample, 1))(input1)
    second = Dense(16)(second)

    adding_model = layers.concatenate([first, second], axis=1)
    adding_model = Dense(32)(adding_model)
    adding_model = Dense(1)(adding_model)

    model = Model(inputs=input1, outputs=adding_model)

    eval_model(
        ds_dict,
        ds_name=key,
        ds_split_ratio=ds_split_ratio,
        scaler=MinMaxScaler(feature_range=(0, 1)),
        train_ratio=0.95,
        length_of_sample=length_of_sample,
        model=model,
        opt='adam',
        bs=bs,
        epochs=20,
        model_name='ensemble-basic')

    # Evaluate Hybrid model
    length_of_sample = 60

    model = Sequential()

    model.add(Bidirectional(LSTM(128, return_sequences=False), input_shape=(length_of_sample, 11)))
    model.add(Dense(16))
    model.add(Dense(1))

    bs = 100
    if key.startswith('daily'):
        bs = 10

    ds_split_ratio = 0.6
    if key.startswith('daily') or key.startswith('60min'):
        ds_split_ratio = 0.1

    eval_model(
        ds_dict,
        ds_name=key,
        ds_split_ratio=ds_split_ratio,
        scaler=MinMaxScaler(feature_range=(0, 1)),
        train_ratio=0.95,
        length_of_sample=length_of_sample,
        model=model,
        opt='adam',
        bs=bs,
        epochs=20,
        prepare_dataset_function=prepare_dataset_with_indicators,
        model_name='hybrid'
    )

    # Evaluate Hybrid model with only simple LSTM modules
    length_of_sample = 1

    model = Sequential()

    model.add(LSTM(128, return_sequences=False, input_shape=(length_of_sample, 11)))
    model.add(Dense(32))
    model.add(Dense(1))

    eval_model(
        ds_dict,
        ds_name=key,
        ds_split_ratio=ds_split_ratio,
        scaler=MinMaxScaler(feature_range=(0, 1)),
        train_ratio=0.95,
        length_of_sample=length_of_sample,
        model=model,
        opt='adam',
        bs=bs,
        epochs=30,
        prepare_dataset_function=prepare_dataset_with_indicators,
        model_name='hybrid-basic'
    )

    print('evaluating done ' + key)

#%%
