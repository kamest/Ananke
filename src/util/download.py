import time
from datetime import datetime

import numpy as np
from binance.client import Client
import pandas as pd
from dateutil.relativedelta import relativedelta
import yfinance as yf

# 20 of TOP stocks contained by SP500
# 4 well known ETFS -
#   VEU - Vanguard FTSE All-World ex-US ETF
#   VOO - Vanguard S&P 500 ETF
tickers = ['AAPL', 'AMZN', 'JPM', 'VEU', 'VOO']

crypto_tickers = ['BTC', 'ETH']

columns_to_keep = ['Date', 'Open', 'Close', 'Open Time', 'open', 'close', 'time']
final_columns = ['date', 'open', 'close']


# Normalize column names and invert if needed
# to force datasets to have same format
def normalize_dataset(df, invert_sort):
    actual_columns_to_keep = [value for value in df.columns if value in columns_to_keep]
    df = df[actual_columns_to_keep]
    df.columns = final_columns
    if invert_sort:
        df = df.iloc[::-1]
    return df


# self-explaining
def get_default_or_ticker(default_ticker_set, only_ticker):
    final_tickers = default_ticker_set
    if only_ticker is not None:
        final_tickers = [only_ticker]
    return final_tickers


# constructs url of alphavantage
def construct_url(month_slice, api_key, stock, interval, output_size, function_name):
    url = 'https://www.alphavantage.co/query'
    url += '?function=' + function_name
    url += '&symbol=' + stock
    if output_size is not None:
        url += '&outputsize=' + output_size
        url += '&datatype=csv'
    if interval is not None:
        url += '&interval=' + interval
    if month_slice is not None:
        url += '&slice=' + month_slice
    url += '&apikey=' + api_key
    return url


# Downloads intraday data by block of 2years and 12 months
def download_intraday_data(interval, api_key, only_ticker):

    # api has limits - need to scrape it down
    calls = 0
    for stock in get_default_or_ticker(crypto_tickers, only_ticker):
        df = pd.DataFrame()
        # for two years
        for y in range(1, 3):
            # for twelve months
            for m in range(1, 13):
                print('Downloading ' + stock + ' ' + interval + ' year : ' + str(y) + ' and month: ' + str(m))
                if calls == 5:
                    # calls is reached, need to wait, because api would return false data.
                    print('Waiting  2 mins : ' + stock + ' ' + interval + ' year : ' + str(y) + ' and month: ' + str(m))
                    time.sleep(2 * 60)  # Delay for 2 minutes (2*60 seconds).
                    calls = 0
                # download data
                new_df = pd.read_csv(
                    construct_url(
                        'year' + str(y) + 'month' + str(m),
                        api_key,
                        stock,
                        interval,
                        None,
                        'TIME_SERIES_INTRADAY_EXTENDED')
                )
                print('Received : ' + str(new_df.size) + ' by interval ' + interval)
                df = pd.concat([df, new_df])
                calls += 1
        print(stock + ' downloaded completely in interval ' + interval + '.')
        df = normalize_dataset(df, True)
        # store the data
        df.to_csv('data/' + interval + '/' + stock + '.csv')


# Download daily data from Yahoo
def download_daily_data(only_ticker):

    for ticker in get_default_or_ticker((tickers + crypto_tickers), only_ticker):
        # download
        print('Downloading ' + ticker)
        examined_ticker = ticker
        if ticker in crypto_tickers:
            examined_ticker += "-EUR"
        df = yf.download(examined_ticker, interval='1d')
        # normalize
        df = df.reset_index(level=0)
        df = normalize_dataset(df, False)
        print('Received : ' + str(df.size) + ' for ticker ' + ticker)
        # save the data
        df.to_csv('data/daily/' + ticker + '.csv')


# download intraday crypto data
def download_crypto_data(interval, api_key, api_secret, only_ticker):
    client = Client(api_key, api_secret)
    for ticker in get_default_or_ticker(crypto_tickers, only_ticker):
        df = pd.DataFrame()
        # tries to download as far history as possible - maximum 40 months
        # but usually only 15 months are provided
        for minus_months in range(1, 40):
            # download current month
            print('Downloading ' + ticker + ' ' + interval + ' month: ' + str(minus_months))
            from_date = datetime.today() - relativedelta(months=minus_months)
            to_date = datetime.today() - relativedelta(months=minus_months-1)
            print(from_date)
            df_data = np.array(
                client.get_historical_klines(
                    symbol=ticker + 'EUR',
                    interval=interval,
                    start_str=int(from_date.timestamp() * 1000),
                    end_str=int(to_date.timestamp()*1000)
                )
            ).reshape(-1, 12)

            # normalize
            print(len(df_data))
            new_df = pd.DataFrame(
                data=df_data,
                dtype=float,
                columns=('Open Time',
                         'Open',
                         'High',
                         'Low',
                         'Close',
                         'Volume',
                         'Close time',
                         'Quote asset volume',
                         'Number of trades',
                         'Taker buy base asset volume',
                         'Taker buy quote asset volume',
                         'Ignore')
            )

            new_df['Open Time'] = pd.to_datetime(new_df['Open Time'], unit='ms')
            new_df = new_df.iloc[::-1]
            df = pd.concat([df, new_df])

        print(ticker + ' downloaded completely in interval ' + interval + '.')
        df = normalize_dataset(df, True)
        # save the data
        if interval == Client.KLINE_INTERVAL_1HOUR:
            df.to_csv('data/60min-new/' + ticker + '.csv')
        elif interval == Client.KLINE_INTERVAL_1DAY:
            df.to_csv('data/daily/' + ticker + '.csv')
        else:
            df.to_csv('data/' + interval + 'in/' + ticker + '.csv')
