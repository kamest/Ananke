import json
import os

from keras.metrics import *
from pandas import DataFrame
from sklearn.metrics import r2_score
from sklearn.preprocessing import MinMaxScaler
import statsmodels.api as sm
from statsmodels.tsa.arima.model import ARIMA
from tensorflow import keras

import util.evaluation as evaluation
from util.evaluation import *
from util.util import plot_two_series


# adds "history" to each observation.
# reshape into [n:length_of_samples]
def reshape_for_evaluation(data, length_of_sample):
    close_data = np.array(data['close'])
    x_train = []
    y_train = []

    for i in range(length_of_sample, len(data)):
        x_train.append(data[i - length_of_sample:i])
        y_train.append(close_data[i])

    x_train, y_train = np.array(x_train), np.array(y_train).reshape(-1, 1)

    x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], x_train.shape[2]))
    return x_train, y_train


# splits into train and test set
# specify observation values and results (x,y)
def prepare_for_nn(scaled_df, train_test_ratio, length_of_sample):
    train_size = int(np.ceil(len(scaled_df)*train_test_ratio))
    train_data = scaled_df[0:train_size]
    x_test = scaled_df[train_size-length_of_sample:]
    y_test = np.array(scaled_df['close'][train_size:]).reshape(-1, 1)

    x_train, y_train = reshape_for_evaluation(data=train_data, length_of_sample=length_of_sample)
    return x_train, y_train, x_test, y_test


# prepares only test set
def transform_for_predictions(x_test, y_test, length_of_sample):
    x_test_values, _ = reshape_for_evaluation(x_test, length_of_sample)
    y_test_values = np.array(y_test).reshape(-1, 1)
    return x_test_values, y_test_values


# returns limited dataset transformed into dataframe.
def prepare_dataset(ds_dict, ds_name, ds_split_ratio):
    df = ds_dict[ds_name]
    df = df[int(len(df)*ds_split_ratio):]
    print('evaluating on : ' + str(len(df)))
    df_close = DataFrame({'close': df['close']})
    return df, df_close


# Evaluates the model see:  {#eval_model_with_results}
def eval_model(
        ds_dict,
        ds_name,
        ds_split_ratio,
        scaler,
        train_ratio,
        length_of_sample,
        model,
        opt,
        bs,
        epochs,
        shuffle=True,
        fit_callbacks=None,
        prepare_dataset_function=prepare_dataset,
        model_name=None,
        train_new_model=False):
    eval_model_with_results(
        ds_dict,
        ds_name,
        ds_split_ratio,
        scaler,
        train_ratio,
        length_of_sample,
        model,
        opt,
        bs,
        epochs,
        shuffle=shuffle,
        fit_callbacks=fit_callbacks,
        prepare_dataset_function=prepare_dataset_function,
        model_name=model_name,
        train_new_model=train_new_model
    )


# Evaluates neural network model.
def eval_model_with_results(
        ds_dict,
        ds_name,
        ds_split_ratio,
        scaler,
        train_ratio,
        length_of_sample,
        model,
        opt,
        bs,
        epochs,
        shuffle=True,
        fit_callbacks=None,
        prepare_dataset_function=prepare_dataset,
        model_name=None,
        train_new_model=False):

    if fit_callbacks is None:
        fit_callbacks = []

    # Load the model if is possible to cut the train time
    model_id = None
    model_loaded = False
    if model_name is not None:
        model_id = ds_name + '-' + model_name
        model_path = 'models/' + model_id + '.h5'
        if os.path.exists(model_path) and not train_new_model:
            model = keras.models.load_model(model_path)
            model_loaded = True

    # prepare dataset
    df, df_close = prepare_dataset_function(ds_dict, ds_name, ds_split_ratio)

    # scale df and split into train/test sets
    df_close['close'] = scaler.fit_transform(np.array(np.nan_to_num(df_close['close'])).reshape(-1, 1))
    x_train, y_train, x_test, y_test = prepare_for_nn(df_close, train_ratio, length_of_sample)

    if not model_loaded:
        # if not pre-trained compile and train
        model.compile(
            optimizer=opt,
            loss='mean_squared_error',
            metrics=[RootMeanSquaredError(), MeanAbsoluteError(), MeanSquaredError()])

        model.fit(x_train, y_train, batch_size=bs, epochs=epochs, shuffle=shuffle, callbacks=fit_callbacks)

    # predict and evaluate
    x_test_values, y_test_values = transform_for_predictions(x_test, y_test, length_of_sample)
    predictions = model.predict(x_test_values)
    evaluations = model.evaluate(x_test_values, y_test_values)

    # retain score of evaluations
    print('Evaluations for model on dataset ' + ds_name)
    local_r2 = str(r2_score(y_test_values, predictions))
    print('r^2 score : ' + local_r2)
    local_rmse = str(evaluations[1])
    print('rmse: ' + local_rmse)
    local_mae = str(evaluations[2])
    print('mae: ' + local_mae)
    local_mse = str(evaluations[3])
    print('mse: ' + local_mse)

    # save results and possibly save the trained model
    if model_id is not None:
        results = {
            ds_name + '-' + model_name + '-r2': local_r2,
            ds_name + '-' + model_name + '-rmse': local_rmse,
            ds_name + '-' + model_name + '-mae': local_mae,
            ds_name + '-' + model_name + '-mse': local_mse,
            ds_name + '-' + model_name + '-test_length': len(predictions)
        }
        with open('data-results/' + ds_name+'-'+model_name + 'data.json', 'w') as fp:
            json.dump(results, fp,  indent=4)
        if not model_loaded:
            model.save('models/' + model_id + '.h5')

    # de-normalize model
    norm_predictions = scaler.inverse_transform(predictions)
    norm_test_values = scaler.inverse_transform(y_test_values)

    # plot results
    plot_two_series(
        norm_predictions,
        'Predikce',
        norm_test_values,
        'Reálná hodnota',
        df['date'],
        model_id
    )
    plot_two_series(
        norm_predictions[-30:],
        'Predikce',
        norm_test_values[-30:],
        'Reálná hodnota',
        df['date'][-30:],
        model_id+'-cut' if model_id is not None else None
    )

    # calculate how profitable model is
    calculate_profit(norm_test_values, norm_predictions, ds_name, model_name)
    return predictions, x_train, y_train, x_test_values, y_test_values, evaluations


# Evaluates the same way as neural network - `eval_model_with_results`
def evaluate_arima(ds_dict, ds_name, ds_split_ratio):
    # prepare dataset
    df, df_close = prepare_dataset(ds_dict, ds_name, ds_split_ratio)
    scaler = MinMaxScaler(feature_range=(-1, 1))
    df_close = scaler.fit_transform(np.array(df_close).reshape(-1, 1))
    # differentiate dataset
    ts = np.diff(np.array(df_close[:, 0]))
    ts = np.array(ts)

    # show ACF and PACF function plots to guess p ARIMA parameter
    fig = plt.figure(figsize=(12, 8))
    ax1 = fig.add_subplot(211)
    fig = sm.graphics.tsa.plot_acf(ts.squeeze(), lags=10, ax=ax1)
    ax2 = fig.add_subplot(212)
    sm.graphics.tsa.plot_pacf(ts, lags=5, ax=ax2)

    # split into train/test sets
    train_size = int(len(df_close) * 0.80)
    train, test = df_close[0:train_size], df_close[train_size:len(df_close)]
    history = [x for x in train]
    predictions = list()
    for t in range(len(test)):
        # train model
        model = ARIMA(history, order=(1, 1, 0))
        # predict
        model_fit = model.fit(method_kwargs={'warn_convergence': False})
        predictions.append(model_fit.forecast()[0])
        # add current value to history for next prediction
        history.append(test[t])
        if t % 10 == 0:
            print('Processing : ' + str(t) + ' of: ' + str(len(test)))

    # denormalize values
    predictions_denormalized = scaler.inverse_transform(np.array(predictions).reshape(-1, 1))
    real_value_denormalized = scaler.inverse_transform(np.array(test).reshape(-1, 1))

    # plot results
    plot_two_series(
        predictions_denormalized,
        'Predikce',
        real_value_denormalized,
        'Reálná hodnota',
        df['date'][train_size:],
        ds_name+'-arima')
    plot_two_series(
        predictions_denormalized[-30:],
        'Predikce',
        real_value_denormalized[-30:],
        'Reálná hodnota',
        df['date'][-30:],
        ds_name+'-arima'+'-cut')

    # compute manually metrics
    print(root_mean_squared_error(test, predictions))
    print(evaluation.mean_squared_error(test, predictions))
    print(evaluation.mean_absolute_error(test, predictions))
    print(r2_score(test, predictions))

    # print metrics
    local_r2 = str(r2_score(test, predictions))
    print('r^2 score : ' + local_r2)
    local_rmse = str(root_mean_squared_error(test, predictions))
    print('rmse: ' + local_rmse)
    local_mae = str(evaluation.mean_absolute_error(test, predictions))
    print('mae: ' + local_mae)
    local_mse = str(evaluation.mean_squared_error(test, predictions))
    print('mse: ' + local_mse)

    # save metrics
    results = {
            ds_name + '-arima-r2': local_r2,
            ds_name + '-arima-rmse': local_rmse,
            ds_name + '-arima-mae': local_mae,
            ds_name + '-arima-mse': local_mse,
            ds_name + '-arima-test_length': len(predictions_denormalized)}
    with open('data-results/' + ds_name+'-arimadata.json', 'w') as fp:
        json.dump(results, fp,  indent=4)

    # calculate how profitable model is
    calculate_profit(real_value_denormalized, predictions_denormalized, ds_name, 'arima')
